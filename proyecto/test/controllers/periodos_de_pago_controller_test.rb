require 'test_helper'

class PeriodosDePagoControllerTest < ActionController::TestCase
  setup do
    @periodo_de_pago = periodos_de_pago(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:periodos_de_pago)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create periodo_de_pago" do
    assert_difference('PeriodoDePago.count') do
      post :create, periodo_de_pago: { anho: @periodo_de_pago.anho, finalizado: @periodo_de_pago.finalizado, mes: @periodo_de_pago.mes }
    end

    assert_redirected_to periodo_de_pago_path(assigns(:periodo_de_pago))
  end

  test "should show periodo_de_pago" do
    get :show, id: @periodo_de_pago
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @periodo_de_pago
    assert_response :success
  end

  test "should update periodo_de_pago" do
    patch :update, id: @periodo_de_pago, periodo_de_pago: { anho: @periodo_de_pago.anho, finalizado: @periodo_de_pago.finalizado, mes: @periodo_de_pago.mes }
    assert_redirected_to periodo_de_pago_path(assigns(:periodo_de_pago))
  end

  test "should destroy periodo_de_pago" do
    assert_difference('PeriodoDePago.count', -1) do
      delete :destroy, id: @periodo_de_pago
    end

    assert_redirected_to periodos_de_pago_path
  end
end
