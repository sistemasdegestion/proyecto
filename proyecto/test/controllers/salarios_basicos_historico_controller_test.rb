require 'test_helper'

class SalariosBasicosHistoricoControllerTest < ActionController::TestCase
  setup do
    @salario_basico_historico = salarios_basicos_historico(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:salarios_basicos_historico)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create salario_basico_historico" do
    assert_difference('SalarioBasicoHistorico.count') do
      post :create, salario_basico_historico: { categoria_id: @salario_basico_historico.categoria_id, fecha: @salario_basico_historico.fecha, monto: @salario_basico_historico.monto }
    end

    assert_redirected_to salario_basico_historico_path(assigns(:salario_basico_historico))
  end

  test "should show salario_basico_historico" do
    get :show, id: @salario_basico_historico
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @salario_basico_historico
    assert_response :success
  end

  test "should update salario_basico_historico" do
    patch :update, id: @salario_basico_historico, salario_basico_historico: { categoria_id: @salario_basico_historico.categoria_id, fecha: @salario_basico_historico.fecha, monto: @salario_basico_historico.monto }
    assert_redirected_to salario_basico_historico_path(assigns(:salario_basico_historico))
  end

  test "should destroy salario_basico_historico" do
    assert_difference('SalarioBasicoHistorico.count', -1) do
      delete :destroy, id: @salario_basico_historico
    end

    assert_redirected_to salarios_basicos_historico_path
  end
end
