require 'test_helper'

class TrabajadoresControllerTest < ActionController::TestCase
  setup do
    @trabajador = trabajadores(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:trabajadores)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create trabajador" do
    assert_difference('Trabajador.count') do
      post :create, trabajador: { categoria_id: @trabajador.categoria_id, ciudad: @trabajador.ciudad, direccion: @trabajador.direccion, documento_de_identidad: @trabajador.documento_de_identidad, email: @trabajador.email, estado_id: @trabajador.estado_id, fecha_de_ingreso: @trabajador.fecha_de_ingreso, nacionalidad: @trabajador.nacionalidad, nombre: @trabajador.nombre, numero_de_telefono: @trabajador.numero_de_telefono }
    end

    assert_redirected_to trabajador_path(assigns(:trabajador))
  end

  test "should show trabajador" do
    get :show, id: @trabajador
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @trabajador
    assert_response :success
  end

  test "should update trabajador" do
    patch :update, id: @trabajador, trabajador: { categoria_id: @trabajador.categoria_id, ciudad: @trabajador.ciudad, direccion: @trabajador.direccion, documento_de_identidad: @trabajador.documento_de_identidad, email: @trabajador.email, estado_id: @trabajador.estado_id, fecha_de_ingreso: @trabajador.fecha_de_ingreso, nacionalidad: @trabajador.nacionalidad, nombre: @trabajador.nombre, numero_de_telefono: @trabajador.numero_de_telefono }
    assert_redirected_to trabajador_path(assigns(:trabajador))
  end

  test "should destroy trabajador" do
    assert_difference('Trabajador.count', -1) do
      delete :destroy, id: @trabajador
    end

    assert_redirected_to trabajadores_path
  end
end
