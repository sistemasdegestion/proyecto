require 'test_helper'

class AsientosContablesControllerTest < ActionController::TestCase
  setup do
    @asiento_contable = asientos_contables(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:asientos_contables)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create asiento_contable" do
    assert_difference('AsientoContable.count') do
      post :create, asiento_contable: { concepto: @asiento_contable.concepto, ejercicio_contable_id: @asiento_contable.ejercicio_contable_id, fecha: @asiento_contable.fecha, numero: @asiento_contable.numero, total_debe: @asiento_contable.total_debe, total_haber: @asiento_contable.total_haber }
    end

    assert_redirected_to asiento_contable_path(assigns(:asiento_contable))
  end

  test "should show asiento_contable" do
    get :show, id: @asiento_contable
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @asiento_contable
    assert_response :success
  end

  test "should update asiento_contable" do
    patch :update, id: @asiento_contable, asiento_contable: { concepto: @asiento_contable.concepto, ejercicio_contable_id: @asiento_contable.ejercicio_contable_id, fecha: @asiento_contable.fecha, numero: @asiento_contable.numero, total_debe: @asiento_contable.total_debe, total_haber: @asiento_contable.total_haber }
    assert_redirected_to asiento_contable_path(assigns(:asiento_contable))
  end

  test "should destroy asiento_contable" do
    assert_difference('AsientoContable.count', -1) do
      delete :destroy, id: @asiento_contable
    end

    assert_redirected_to asientos_contables_path
  end
end
