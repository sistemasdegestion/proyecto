require 'test_helper'

class LiquidacionDetallesControllerTest < ActionController::TestCase
  setup do
    @liquidacion_detalle = liquidacion_detalles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:liquidacion_detalles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create liquidacion_detalle" do
    assert_difference('LiquidacionDetalle.count') do
      post :create, liquidacion_detalle: { cantidad: @liquidacion_detalle.cantidad, concepto_id: @liquidacion_detalle.concepto_id, descuento: @liquidacion_detalle.descuento, liquidacion_id: @liquidacion_detalle.liquidacion_id, suma: @liquidacion_detalle.suma }
    end

    assert_redirected_to liquidacion_detalle_path(assigns(:liquidacion_detalle))
  end

  test "should show liquidacion_detalle" do
    get :show, id: @liquidacion_detalle
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @liquidacion_detalle
    assert_response :success
  end

  test "should update liquidacion_detalle" do
    patch :update, id: @liquidacion_detalle, liquidacion_detalle: { cantidad: @liquidacion_detalle.cantidad, concepto_id: @liquidacion_detalle.concepto_id, descuento: @liquidacion_detalle.descuento, liquidacion_id: @liquidacion_detalle.liquidacion_id, suma: @liquidacion_detalle.suma }
    assert_redirected_to liquidacion_detalle_path(assigns(:liquidacion_detalle))
  end

  test "should destroy liquidacion_detalle" do
    assert_difference('LiquidacionDetalle.count', -1) do
      delete :destroy, id: @liquidacion_detalle
    end

    assert_redirected_to liquidacion_detalles_path
  end
end
