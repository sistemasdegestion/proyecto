require 'test_helper'

class NovedadesTransitoriasControllerTest < ActionController::TestCase
  setup do
    @novedad_transitoria = novedades_transitorias(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:novedades_transitorias)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create novedad_transitoria" do
    assert_difference('NovedadTransitoria.count') do
      post :create, novedad_transitoria: { cantidad: @novedad_transitoria.cantidad, concepto_id_integer: @novedad_transitoria.concepto_id_integer, descuento: @novedad_transitoria.descuento, liquidacion_id: @novedad_transitoria.liquidacion_id, suma: @novedad_transitoria.suma }
    end

    assert_redirected_to novedad_transitoria_path(assigns(:novedad_transitoria))
  end

  test "should show novedad_transitoria" do
    get :show, id: @novedad_transitoria
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @novedad_transitoria
    assert_response :success
  end

  test "should update novedad_transitoria" do
    patch :update, id: @novedad_transitoria, novedad_transitoria: { cantidad: @novedad_transitoria.cantidad, concepto_id_integer: @novedad_transitoria.concepto_id_integer, descuento: @novedad_transitoria.descuento, liquidacion_id: @novedad_transitoria.liquidacion_id, suma: @novedad_transitoria.suma }
    assert_redirected_to novedad_transitoria_path(assigns(:novedad_transitoria))
  end

  test "should destroy novedad_transitoria" do
    assert_difference('NovedadTransitoria.count', -1) do
      delete :destroy, id: @novedad_transitoria
    end

    assert_redirected_to novedades_transitorias_path
  end
end
