require 'test_helper'

class LogAuditoriasControllerTest < ActionController::TestCase
  setup do
    @log_autitoria = log_auditorias(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:log_auditorias)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create log_autitoria" do
    assert_difference('LogAutitoria.count') do
      post :create, log_autitoria: { action: @log_autitoria.action, registro: @log_autitoria.registro, role: @log_autitoria.role, table: @log_autitoria.table, user_name: @log_autitoria.user_name }
    end

    assert_redirected_to log_autitoria_path(assigns(:log_autitoria))
  end

  test "should show log_autitoria" do
    get :show, id: @log_autitoria
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @log_autitoria
    assert_response :success
  end

  test "should update log_autitoria" do
    patch :update, id: @log_autitoria, log_autitoria: { action: @log_autitoria.action, registro: @log_autitoria.registro, role: @log_autitoria.role, table: @log_autitoria.table, user_name: @log_autitoria.user_name }
    assert_redirected_to log_autitoria_path(assigns(:log_autitoria))
  end

  test "should destroy log_autitoria" do
    assert_difference('LogAutitoria.count', -1) do
      delete :destroy, id: @log_autitoria
    end

    assert_redirected_to log_auditorias_path
  end
end
