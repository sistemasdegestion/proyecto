class Liquidacion < ActiveRecord::Base
  has_many :liquidacion_detalles, :dependent => :destroy
  has_many :novedades_transitorias, :dependent => :destroy
  accepts_nested_attributes_for :liquidacion_detalles
  accepts_nested_attributes_for :novedades_transitorias
  belongs_to :trabajador
  belongs_to :categoria
  belongs_to :periodo_de_pago
  before_create :agregar_categoria

  def agregar_categoria
    self.categoria_id = self.trabajador.categoria_id
  end

  def prueba
    #$salario_basico = @cat.salario_basico
    #Liquidacion.where(trabajador_id: self.trabajador_id, periodo_de_pago_id: PeriodoDePago.where(anho: @anho).id).find_each do |liquidacion|
    #  $acumulado = $acumulado + liquidacion.total
    #end
    cargar_variables
    @trab = Trabajador.find(self.trabajador_id)
    @cat = Categoria.find(@trab.categoria_id)
    self.categoria_id = @cat.id
    #SalarioBasicoHistorico.create(categoria_id: @cat.id, monto: 1010, fecha: Date.today)
  end

  def concept
    Concepto.order(codigo: :asc).fijo.each do |concepto|
      LiquidacionDetalle.create(liquidacion_id: self.id, concepto_id: concepto.id, cantidad: 1, suma: 0, descuento: 0)
    end

  end
  def cargar_variables
    $A1 = 0
    $A2 = 0
    $A3 = 0
    $A4 = 0
    $A5 = 0
    $AT = 0
    $acumulado = 0
    $liq_en_el_anho = 0
    $antiguedad = Time.diff(Date.tomorrow, self.trabajador.fecha_de_ingreso)
    @trab = Trabajador.find(self.trabajador_id)
    @cat = Categoria.find(@trab.categoria_id)
    @periodself = PeriodoDePago.find(self.periodo_de_pago_id)
    Liquidacion.where(trabajador_id: self.trabajador_id).find_each do |liq|
      if liq.id != self.id
        @period2 = PeriodoDePago.find(liq.periodo_de_pago_id)
        if @periodself.anho == @period2.anho
          $acumulado = $acumulado + liq.total
          $liq_en_el_anho = $liq_en_el_anho + 1
        end
      end
    end
    200.times do |i|
      @c = "c" + i.to_s
      $calculadora.store(@c => 0)
    end
    $calculadora.store(antiguedad: $antiguedad[:year].to_i)
    $calculadora.store(salario_basico: @cat.salario_basico)
    $calculadora.store(acumulado: $acumulado)
    $calculadora.store(liq_en_el_anho: $liq_en_el_anho)
  end

  def cargar_variables_transitorias

  end
end
