class SalarioBasicoHistorico < ActiveRecord::Base
  has_one :categoria
  self.per_page = 2

  validates :categoria_id, presence: { message: " es requerido"}

  def self.search(search)
    if search
      search_condition = (search).to_i
      where('categoria_id ==?', search_condition)
    else
      default_scoped
    end
  end
end
