class AsientoDetalle < ActiveRecord::Base
  after_create :sumar_al_total
  def sumar_al_total
    @asiento = AsientoContable.find(self.asiento_id)
    AsientoContable.update(@asiento.id, total_debe: (@asiento.total_debe + self.debe), total_haber: (@asiento.total_haber + self.haber))
  end
end
