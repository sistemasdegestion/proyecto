class Trabajador < ActiveRecord::Base
  has_many :liquidacions
  belongs_to :categoria
  belongs_to :nacionalidad


  def nombre_apellido
    "#{nombre} #{apellido}"
  end
  private


end
