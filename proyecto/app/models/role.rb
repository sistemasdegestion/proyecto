class Role < ActiveRecord::Base
  has_many :users
  has_many :permissions, :dependent => :destroy
  accepts_nested_attributes_for :permissions

  def set_permissions(permissions)
    permissions.each do |id|
      #find the main permission assigned from the UI
      permission = Permission.find(id)
      self.permissions << permission
      case permission.subject_class
        when "Part"
          case permission.action
            #if create part permission is assigned then assign create drawing as well
            when "create"
              self.permissions << Permission.where(subject_class: "Drawing", action: "create")
            #if update part permission is assigned then assign create and delete drawing as well
            when "update"
              self.permissions << Permission.where(subject_class: "Drawing", action: ["update", "destroy"])
          end
      end
    end
  end
end
