class PeriodoDePago < ActiveRecord::Base
  has_many :liquidacions

  def mes_anho
    "#{mes} - #{anho}"
  end
  #after_create :liquidar

  def liquidar
    Trabajador.all.find_each do |trabajador|
      if Liquidacion.where(trabajador_id: trabajador.id, periodo_de_pago_id: self.id).first == nil and trabajador.estado == 'activo'
        Liquidacion.create(trabajador_id: trabajador.id, categoria_id: trabajador.categoria_id, periodo_de_pago_id: self.id, fecha_de_pago: DateTime.now.to_date, total: 0)
      end
    end
  end
end
