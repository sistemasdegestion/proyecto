class Permission < ActiveRecord::Base
  belongs_to :role
  after_create :auditar_crear
  after_update :auditar_editar

  def auditar_crear
    LogAutitoria.create(user_name: $current_user.email, role: $current_user.role.name, action: "Crear", table: "Permisos", registro: self.id, tiempo: Time.now, cambios: ["Tabla: " + self.subject_class.to_s, "Permisos: " + self.actions.join(", ")])
  end

  def auditar_editar
    LogAutitoria.create(user_name: $current_user.email, role: $current_user.role.name, action: "Editar", table: "Permisos", registro: self.id, tiempo: Time.now, cambios: ["Tabla: " + self.subject_class.to_s, "Permisos: " + self.actions.join(", ")])
  end
end