class Concepto < ActiveRecord::Base
  scope :fijo, -> { where(tipo_de_concepto: 'fijo') }
  has_many :liquidacion_detalles
  has_one :acumulador
  validates_uniqueness_of :nombre

  def codigo_nombre
    "#{codigo} - #{nombre}"
  end
  def acumular
    if self.acumuladores.include?("a1")
      $A1 = $A1 + $calculadora.evaluate(self.formula) + self.monto
      $calculadora.store(A1: $A1)
    end
    if self.acumuladores.include?("a2")
      $A2 = $A2 + $calculadora.evaluate(self.formula) + self.monto
      $calculadora.store(A2: $A2)
    end
    if self.acumuladores.include?("a3")
      $A3 = $A3 + $calculadora.evaluate(self.formula) + self.monto
      $calculadora.store(A3: $A3)
    end
    if self.acumuladores.include?("a4")
      $A4 = $A4 + $calculadora.evaluate(self.formula) + self.monto
      $calculadora.store(A4: $A4)
    end
    if self.acumuladores.include?("a5")
      $A5 = $A5 + $calculadora.evaluate(self.formula) + self.monto
      $calculadora.store(A5: $A5)
    end
    if self.suma_resta == 'suma'
      $AT = $AT + $calculadora.evaluate(self.formula) + self.monto
    else
      $AT = $AT - $calculadora.evaluate(self.formula) - self.monto
    end
    $calculadora.store(AT: $AT)
  end
end
