class LiquidacionDetalle < ActiveRecord::Base
  belongs_to :liquidacion
  belongs_to :concepto
  before_create :calcular

  def calcular
    if self.concepto.suma_resta == "suma" then
      self.suma = $calculadora.evaluate(self.concepto.formula)*self.cantidad + self.concepto.monto*self.cantidad
    elsif self.concepto.suma_resta == "resta" then
      self.descuento = $calculadora.evaluate(self.concepto.formula)*self.cantidad + self.concepto.monto*self.cantidad
    end
    acumular
    Liquidacion.update(self.liquidacion_id, total: self.liquidacion.total + self.suma - self.descuento)
  end
  def acumular
    if self.concepto.acumuladores.include?("a1")
      $A1 = $A1 + self.suma + self.descuento
      $calculadora.store(a1: $A1)
    end
    if self.concepto.acumuladores.include?("a2")
      $A2 = $A2 + self.suma + self.descuento
      $calculadora.store(a2: $A2)
    end
    if self.concepto.acumuladores.include?("a3")
      $A3 = $A3 + self.suma + self.descuento
      $calculadora.store(a3: $A3)
    end
    if self.concepto.acumuladores.include?("a4")
      $A4 = $A4 + self.suma + self.descuento
      $calculadora.store(a4: $A4)
    end
    if self.concepto.acumuladores.include?("a5")
      $A5 = $A5 + self.suma + self.descuento
      $calculadora.store(a5: $A5)
    end
    if self.concepto.suma_resta == 'suma'
      $AT = $AT + self.suma + self.descuento
    else
      $AT = $AT - self.suma - self.descuento
    end
    $calculadora.store(at: $AT)
    @c = "c" + self.concepto.codigo
    $calculadora.store(@c => (self.descuento + self.suma))
  end
end