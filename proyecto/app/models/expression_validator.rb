class ExpressionValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    begin
      Dentaku(value)
    rescue Exception => e
      record.errors[attribute] << (options[:message] || e.message)
    end
  end
end