class User < ActiveRecord::Base
  belongs_to :role
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :timeoutable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  def permisos_array
    @permisos = []
    self.role.permissions.find_each do |permisos|
      if permisos.actions.include?('read') or permisos.actions.include?('manage')
        @permisos.push permisos.subject_class
      end
    end
    return @permisos
  end
end