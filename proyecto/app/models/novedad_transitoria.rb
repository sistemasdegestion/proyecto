class NovedadTransitoria < ActiveRecord::Base
  scope :transitorias_ordenadas, -> {
    order(codigo => desc)
  }
  belongs_to :liquidacion
  belongs_to :concepto
  before_create :calcular
  after_create :auditar
  include ActionView::Helpers::NumberHelper

  def calcular
    if self.concepto.suma_resta == "suma" then
      self.suma = $calculadora.evaluate(self.concepto.formula)*self.cantidad + self.concepto.monto*self.cantidad
    elsif self.concepto.suma_resta == "resta" then
      self.descuento = $calculadora.evaluate(self.concepto.formula)*self.cantidad + self.concepto.monto*self.cantidad
    end
    acumular
    Liquidacion.update(self.liquidacion_id, total: self.liquidacion.total + self.suma - self.descuento)
  end

  def auditar
    LogAutitoria.create(user_name: $current_user.email, role: $current_user.role.name, action: "Crear", table: "Novedad Transitoria", registro: self.id, tiempo: Time.now, cambios: ["Liquidacion_id: " + self.liquidacion_id.to_s, "Concepto_id: " + self.concepto_id.to_s, "Cantidad: " + self.cantidad.to_s, "Suma: " + number_to_currency(self.suma, precision: 0, unit: "G.").to_s, "Descuento: " + number_to_currency(self.descuento, precision: 0, unit: "G.").to_s])
  end

  def prueba
    @conc = Concepto.find(self.concepto_id)
    if @conc.suma_resta == "suma" then
      self.suma = $calculadora.evaluate(@conc.formula)*self.cantidad + @conc.monto*self.cantidad
    elsif @conc.suma_resta == "resta" then
      self.descuento = $calculadora.evaluate(@conc.formula)*self.cantidad + @conc.monto*self.cantidad
    end
    acumular
  end

  def acumular
    if self.concepto.acumuladores.include?("a1")
      $A1 = $A1 + self.suma + self.descuento
      $calculadora.store(a1: $A1)
    end
    if self.concepto.acumuladores.include?("a2")
      $A2 = $A2 + self.suma + self.descuento
      $calculadora.store(a2: $A2)
    end
    if self.concepto.acumuladores.include?("a3")
      $A3 = $A3 + self.suma + self.descuento
      $calculadora.store(a3: $A3)
    end
    if self.concepto.acumuladores.include?("a4")
      $A4 = $A4 + self.suma + self.descuento
      $calculadora.store(a4: $A4)
    end
    if self.concepto.acumuladores.include?("a5")
      $A5 = $A5 + self.suma + self.descuento
      $calculadora.store(a5: $A5)
    end
    if self.concepto.suma_resta == 'suma'
      $AT = $AT + self.suma + self.descuento
    else
      $AT = $AT - self.suma - self.descuento
    end
    $calculadora.store(at: $AT)
    @c = "c" + self.concepto.codigo
    $calculadora.store(@c => (self.descuento + self.suma))
  end
end