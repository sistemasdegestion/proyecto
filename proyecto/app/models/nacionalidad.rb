class Nacionalidad < ActiveRecord::Base
  has_many :trabajadors

  validates_uniqueness_of :nombre
end
