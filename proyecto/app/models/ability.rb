class Ability
  include CanCan::Ability

  def initialize(user)

    user.role.permissions.each do |permission|
      if permission.subject_class == "all"
        can permission.actions.map { |x| x.to_sym }, permission.subject_class.to_sym
      else
        can permission.actions.map { |x| x.to_sym }, permission.subject_class.constantize
      end
    end
  end
end
