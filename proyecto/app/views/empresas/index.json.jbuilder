json.array!(@empresas) do |empresa|
  json.extract! empresa, :id, :nombre, :direccion, :correo, :telefono, :image, :remote_image_url
  json.url empresa_url(empresa, format: :json)
end
