json.array!(@conceptos) do |concepto|
  json.extract! concepto, :id, :codigo, :nombre, :monto, :formula, :tipo_de_concepto, :acumulador, :suma_resta
  json.url concepto_url(concepto, format: :json)
end
