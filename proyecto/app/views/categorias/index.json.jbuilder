json.array!(@categorias) do |categoria|
  json.extract! categoria, :id, :nombre, :salario_basico
  json.url categoria_url(categoria, format: :json)
end
