json.array!(@salarios_basicos_historico) do |salario_basico_historico|
  json.extract! salario_basico_historico, :id, :categoria_id, :monto, :fecha
  json.url salario_basico_historico_url(salario_basico_historico, format: :json)
end
