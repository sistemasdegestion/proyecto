json.array!(@asientos_contables) do |asiento_contable|
  json.extract! asiento_contable, :id, :ejercicio_contable_id, :concepto, :fecha, :numero, :total_debe, :total_haber
  json.url asiento_contable_url(asiento_contable, format: :json)
end
