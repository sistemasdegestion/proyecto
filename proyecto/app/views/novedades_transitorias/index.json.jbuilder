json.array!(@novedades_transitorias) do |novedad_transitoria|
  json.extract! novedad_transitoria, :id, :liquidacion_id, :concepto_id_integer, :cantidad, :suma, :descuento
  json.url novedad_transitoria_url(novedad_transitoria, format: :json)
end
