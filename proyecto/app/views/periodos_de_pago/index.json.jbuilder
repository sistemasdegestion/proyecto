json.array!(@periodos_de_pago) do |periodo_de_pago|
  json.extract! periodo_de_pago, :id, :mes, :anho, :finalizado
  json.url periodo_de_pago_url(periodo_de_pago, format: :json)
end
