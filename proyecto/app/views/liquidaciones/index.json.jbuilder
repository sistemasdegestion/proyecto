json.array!(@liquidaciones) do |liquidacion|
  json.extract! liquidacion, :id, :trabajador_id, :categoria_id, :mes, :fecha_de_pago, :total
  json.url liquidacion_url(liquidacion, format: :json)
end
