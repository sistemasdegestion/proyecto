json.array!(@liquidacion_detalles) do |liquidacion_detalle|
  json.extract! liquidacion_detalle, :id, :liquidacion_id, :concepto_id, :cantidad, :suma, :descuento
  json.url liquidacion_detalle_url(liquidacion_detalle, format: :json)
end
