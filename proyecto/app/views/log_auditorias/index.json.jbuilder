json.array!(@log_auditorias) do |log_autitoria|
  json.extract! log_autitoria, :id, :user_name, :role, :action, :table, :registro
  json.url log_autitoria_url(log_autitoria, format: :json)
end
