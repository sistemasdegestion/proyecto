json.array!(@trabajadores) do |trabajador|
  json.extract! trabajador, :id, :nombre, :ciudad, :direccion, :nacionalidad, :documento_de_identidad, :email, :numero_de_telefono, :categoria_id, :fecha_de_ingreso, :estado_id
  json.url trabajador_url(trabajador, format: :json)
end
