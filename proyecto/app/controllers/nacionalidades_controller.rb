class NacionalidadesController < ApplicationController
  before_action :set_categoria, only: [:show, :edit, :update, :destroy]
  helper_method :sort_column, :sort_direction
  before_filter :authenticate_user!
  load_and_authorize_resource except: [:create]
  def index
    @nacionalidades = Nacionalidad.all
    @nacionalidadd = Nacionalidad.new
  end

  # GET /categorias/1
  # GET /categorias/1.json
  def show
    #setHabilitar(true)
    @nacionalidades = Nacionalidad.all
  end

  # GET /categorias/new
  def new
    @nacionalidadd = Nacionalidad.new
  end

  # GET /categorias/1/edit
  def edit
    @nacionalidades = Nacionalidad.all
  end

  # POST /categorias
  # POST /categorias.json
  def create
    @nacionalidadd = Nacionalidad.create(nacionalidad_params)
  end

  # PATCH/PUT /categorias/1
  # PATCH/PUT /categorias/1.json
  def update
    respond_to do |format|
      if @nacionalidadd.update(nacionalidad_params)
        format.html { redirect_to categorias_path, notice: 'La nacionalidad ha sido actualizada correctamente.' }
        format.json { render :show, status: :ok, location: @nacionalidadd }
      else
        format.html { render :edit }
        format.json { render json: @nacionalidadd.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categorias/1
  # DELETE /categorias/1.json
  def destroy
    @nacionalidadd.destroy
    respond_to do |format|
      format.html { redirect_to categorias_path, notice: 'La nacionalidad ha sido eliminada correctamente.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_nacionalidad
    @nacionalidadd = Nacionalidad.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def nacionalidad_params
    params.require(:nacionalidad).permit(:nombre)
  end
end
