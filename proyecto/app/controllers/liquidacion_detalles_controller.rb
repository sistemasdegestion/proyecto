class LiquidacionDetallesController < ApplicationController
  before_action :set_liquidacion_detalle, only: [:show, :edit, :update, :destroy]
  helper_method :sort_column, :sort_direction
  before_filter :authenticate_user!
  load_and_authorize_resource except: [:create]
  # GET /liquidacion_detalles
  # GET /liquidacion_detalles.json
  def index
    # @search = LiquidacionDetalle.search params[:search]
    # @order = @search.order :sort_column + " " + :sort_direction
    @liquidacion_detalles = LiquidacionDetalle.search(params[:search]).order(sort_column + ' ' + sort_direction).paginate(:per_page => 5, :page => params[:page])
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @liquidacion_detalles }
    end
  end

  # GET /liquidacion_detalles/1
  # GET /liquidacion_detalles/1.json
  def show
  end

  # GET /liquidacion_detalles/new
  def new
    @liquidacion_detalle = LiquidacionDetalle.new
  end

  # GET /liquidacion_detalles/1/edit
  def edit
  end

  # POST /liquidacion_detalles
  # POST /liquidacion_detalles.json
  def create
    @liquidacion_detalle = LiquidacionDetalle.new(liquidacion_detalle_params)
    respond_to do |format|
      if @liquidacion_detalle.save
        LogAutitoria.create(user_name: current_user.email, role: current_user.role, action: "Crear", table: "Liquidacion_detalles", registro: @liquidacion_detalle.id)
        format.html { redirect_to @liquidacion_detalle, notice: 'Liquidacion detalle was successfully created.' }
        format.json { render :show, status: :created, location: @liquidacion_detalle }
      else
        format.html { render :new }
        format.json { render json: @liquidacion_detalle.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /liquidacion_detalles/1
  # PATCH/PUT /liquidacion_detalles/1.json
  def update
    respond_to do |format|
      if @liquidacion_detalle.update(liquidacion_detalle_params)
        format.html { redirect_to @liquidacion_detalle, notice: 'Liquidacion detalle was successfully updated.' }
        format.json { render :show, status: :ok, location: @liquidacion_detalle }
      else
        format.html { render :edit }
        format.json { render json: @liquidacion_detalle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /liquidacion_detalles/1
  # DELETE /liquidacion_detalles/1.json
  def destroy
    @liquidacion_detalle.destroy
    respond_to do |format|
      format.html { redirect_to liquidacion_detalles_url, notice: 'Liquidacion detalle was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_liquidacion_detalle
    @liquidacion_detalle = LiquidacionDetalle.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def liquidacion_detalle_params
    params.require(:liquidacion_detalle).permit(:liquidacion_id, :concepto_id, :cantidad, :suma, :descuento)
  end

  def sort_column
    LiquidacionDetalle.column_names.include?(params[:sort])? params[:sort] : 'cantidad'
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
