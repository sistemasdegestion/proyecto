class AsientosContablesController < ApplicationController
  before_action :set_asiento_contable, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @asientos_contables = AsientoContable.all
    respond_with(@asientos_contables)
  end

  def show
    respond_with(@asiento_contable)
  end

  def new
    @asiento_contable = AsientoContable.new
    respond_with(@asiento_contable)
  end

  def edit
  end

  def create
    @asiento_contable = AsientoContable.new(asiento_contable_params)
    @asiento_contable.save
    respond_with(@asiento_contable)
  end

  def update
    @asiento_contable.update(asiento_contable_params)
    respond_with(@asiento_contable)
  end

  def destroy
    @asiento_contable.destroy
    respond_with(@asiento_contable)
  end

  private
    def set_asiento_contable
      @asiento_contable = AsientoContable.find(params[:id])
    end

    def asiento_contable_params
      params.require(:asiento_contable).permit(:ejercicio_contable_id, :concepto, :fecha, :numero, :total_debe, :total_haber)
    end
end
