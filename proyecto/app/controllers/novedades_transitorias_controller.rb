class NovedadesTransitoriasController < ApplicationController
  before_action :set_novedad_transitoria, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  respond_to :html

  def index
    @novedades_transitorias = NovedadTransitoria.all
    respond_with(@novedades_transitorias)
  end

  def show
    respond_with(@novedad_transitoria)
  end

  def new
    @novedad_transitoria = NovedadTransitoria.new
  end

  def edit
  end

  def create
    #@novedad_transitoria = NovedadTransitoria.new(novedad_transitoria_params)
    #@novedad_transitoria.save
    #respond_with(@novedad_transitoria)

    @novedad_transitoria = NovedadTransitoria.new(novedad_transitoria_params)

    respond_to do |format|
      if @novedad_transitoria.save
        LogAutitoria.create(user_name: current_user.email, role: current_user.role, action: "Crear", table: "Novedades Transitorias", registro: @novedad_transitoria.id, tiempo: Time.now, cambios: ["Liquidacion id: " + @novedad_transitoria.liquidacion_id.to_s, "Concepto id:" + @novedad_transitoria.concepto_id.to_s, "Cantidad: " + @novedad_transitoria.cantidad.to_s, "Suma: " + @novedad_transitoria.suma.to_s, "Descuento: " + @novedad_transitoria.descuento.to_s])
        format.html { redirect_to @novedad_transitoria, notice: 'Novedad transitoria was successfully created.' }
        format.json { render :show, status: :created, location: @novedad_transitoria }
      else
        format.html { render :new }
        format.json { render json: @novedad_transitoria.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    #@novedad_transitoria.update(novedad_transitoria_params)
    #respond_with(@novedad_transitoria)
    respond_to do |format|
      if @novedad_transitoria.update(liquidacion_detalle_params)
        format.html { redirect_to @novedad_transitoria, notice: 'Novedad transitoria was successfully updated.' }
        format.json { render :show, status: :ok, location: @novedad_transitoria }
      else
        format.html { render :edit }
        format.json { render json: @novedad_transitoria.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    #@novedad_transitoria.destroy
    #respond_with(@novedad_transitoria)
    LogAutitoria.create(user_name: current_user.email, role: current_user.role, action: "Eliminar", table: "Novedades Transitorias", registro: @novedad_transitoria.id, tiempo: Time.now, cambios: ["Liquidacion id: " + @novedad_transitoria.liquidacion_id.to_s, "Concepto id:" + @novedad_transitoria.concepto_id.to_s, "Cantidad: " + @novedad_transitoria.cantidad.to_s, "Suma: " + @novedad_transitoria.suma.to_s, "Descuento: " + @novedad_transitoria.descuento.to_s])
    @novedad_transitoria.destroy
    respond_to do |format|
      format.html { redirect_to liquidacion_detalles_url, notice: 'Novedad transitoria was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def set_novedad_transitoria
    @novedad_transitoria = NovedadTransitoria.find(params[:id])
  end

  def novedad_transitoria_params
    params.require(:novedad_transitoria).permit(:liquidacion_id, :concepto_id, :cantidad, :suma, :descuento)
  end
end
