class CategoriasController < ApplicationController
  before_action :set_categoria, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  load_and_authorize_resource except: [:create]
  def index
    @categorias = Categoria.all
    @categoria = Categoria.new
  end

  # GET /categorias/1
  # GET /categorias/1.json
  def show
    #setHabilitar(true)
    @categorias = Categoria.all
  end

  # GET /categorias/new
  def new
    @categoria = Categoria.new
  end

  # GET /categorias/1/edit
  def edit
    @categorias = Categoria.all
  end

  # POST /categorias
  # POST /categorias.json
  def create
    @categoria = Categoria.new(categoria_params)
    if @categoria.save
      respond_to do |format|
        LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Crear", table: "Categorías", registro: @categoria.id, tiempo: Time.now, cambios: ["nombre: " + @categoria.nombre, "salario: " + number_to_currency(@categoria.salario_basico, precision: 0, unit: "G.").to_s])
        format.html { redirect_to @categoria, notice: 'La Categoría ha sido creada correctamente.' }
        format.json { render :show, status: :ok, location: @categoria }
        format.js {render :create, status: :ok, location: @categoria}

      end
    else
      respond_to do |format|
        format.html { render :new }
         format.json { render json: @categoria.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categorias/1
  # PATCH/PUT /categorias/1.json
  def update
    respond_to do |format|
      if @categoria.update(categoria_params)
        LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Modificar", table: "Categorías", registro: @categoria.id, tiempo: Time.now, cambios: ["nombre: " + @categoria.nombre, "salario: " + number_to_currency(@categoria.salario_basico, precision: 0, unit: "G.").to_s])
        format.html { redirect_to @categoria, notice: 'La Categoría ha sido actualizada correctamente.' }
        format.json { render :show, status: :ok, location: @categoria }
      else
        format.html { render :edit }
        format.json { render json: @categoria.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categorias/1
  # DELETE /categorias/1.json
  def destroy
    if !Trabajador.where(categoria_id: @categoria.id).any? and !Liquidacion.where(categoria_id: @categoria.id).any?
      LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Eliminar", table: "Categorías", registro: @categoria.id, tiempo: Time.now, cambios: ["nombre: " + @categoria.nombre, "salario: " + number_to_currency(@categoria.salario_basico, precision: 0, unit: "G.").to_s])
      @categoria.destroy
      respond_to do |format|
        format.html { redirect_to categorias_path, notice: 'La Categoría ha sido eliminada correctamente.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to categorias_path, error: 'No se ha podido eliminar categoría, por que tiene una o mas liquidaciones o trabajadores asociados.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_categoria
      @categoria = Categoria.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def categoria_params
      params.require(:categoria).permit(:nombre, :salario_basico)
    end
end
