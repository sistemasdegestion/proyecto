class LogAuditoriasController < ApplicationController
  before_action :set_log_autitoria, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  load_and_authorize_resource except: [:create]

  respond_to :html

  def index
    @log_auditorias = LogAutitoria.all
    respond_with(@log_auditorias)
  end

  def show
    respond_with(@log_autitoria)
  end

  def new
    @log_autitoria = LogAutitoria.new
    respond_with(@log_autitoria)
  end

  def edit
  end

  def create
    @log_autitoria = LogAutitoria.new(log_autitoria_params)
    @log_autitoria.save
    respond_with(@log_autitoria)
  end

  def update
    @log_autitoria.update(log_autitoria_params)
    respond_with(@log_autitoria)
  end

  def destroy
    @log_autitoria.destroy
    respond_with(@log_autitoria)
  end

  private
    def set_log_autitoria
      @log_autitoria = LogAutitoria.find(params[:id])
    end

    def log_autitoria_params
      params.require(:log_autitoria).permit(:user_name, :role, :action, :table, :registro, :cambios)
    end
end
