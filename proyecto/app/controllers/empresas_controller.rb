class EmpresasController < ApplicationController
  before_action :set_empresa, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  load_and_authorize_resource except: [:create]
  respond_to :html

  def index
    @empresas = Empresa.all
    respond_with(@empresas)
  end

  def show
    respond_with(@empresa)
  end

  def new
    @empresa = Empresa.new
    respond_with(@empresa)
  end

  def edit
  end

  def create
    @empresa = Empresa.new(empresa_params)
    @empresa.save
    respond_with(@empresa)
  end

  def update
    LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Modificar", table: "Empresa", registro: nil, tiempo: Time.now, cambios: ["nombre: " + @empresa.nombre, "dirección: " + @empresa.direccion, "correo: " + @empresa.correo, "teléfono: " + @empresa.telefono, "imagen: " + @empresa.image.to_s])
    @empresa.update(empresa_params)
    respond_with(@empresa)
  end

  def destroy
    @empresa.destroy
    respond_with(@empresa)
  end

  private
    def set_empresa
      @empresa = Empresa.find(params[:id])
    end

    def empresa_params
      params.require(:empresa).permit(:nombre, :direccion, :correo, :telefono, :image, :remote_image_url)
    end
end
