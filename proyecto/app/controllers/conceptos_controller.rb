class ConceptosController < ApplicationController
  before_action :set_concepto, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  load_and_authorize_resource except: [:create]
  # GET /conceptos
  # GET /conceptos.json
  def index
    @concepto = Concepto.new
    @conceptos = Concepto.all
  end

  # GET /conceptos/1
  # GET /conceptos/1.json
  def show
    @conceptos = Concepto.all
  end


  # GET /conceptos/new
  def new
    @concepto = Concepto.new
  end

  # GET /conceptos/1/edit
  def edit
    @conceptos = Concepto.all
  end

  # POST /conceptos
  # POST /conceptos.json
  def create
    @concepto = Concepto.new(concepto_params)
    200.times do |i|
      @c = "c" + i.to_s
      $calculadora.store(@c => 0)
    end
    $calculadora.store(liq_en_el_anho: 0)
    $calculadora.store(antiguedad: 0)
    $calculadora.store(acumulado: 0)
    $calculadora.store(salario_basico: 0)
    $calculadora.store(a1: 0)
    $calculadora.store(a2: 0)
    $calculadora.store(a3: 0)
    $calculadora.store(a4: 0)
    $calculadora.store(a5: 0)
    $calculadora.store(at: 0)
    if $calculadora.evaluate(@concepto.formula) == nil
      respond_to do |format|
        format.html { redirect_to conceptos_path, error: 'La fórmula es incorrecta.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        if @concepto.save
          LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Crear", table: "Conceptos", registro: @concepto.id, tiempo: Time.now, cambios: ["Código: " + @concepto.codigo, "Nombre: " + @concepto.nombre, "Monto: " + @concepto.monto.to_s, "Suma-Resta: " + @concepto.suma_resta, "Fórmula: " + @concepto.formula, "Tipo: " + @concepto.tipo_de_concepto, "Acumuladores: " + @concepto.acumuladores.join(", ")])
          format.html { redirect_to @concepto, notice: 'El Concepto ha sido creado correctamente.' }
          format.json { render :show, status: :created, location: @concepto }
        else
          format.html { render :new }
          format.json { render json: @concepto.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /conceptos/1
  # PATCH/PUT /conceptos/1.json
  def update
    @formula = (params[:concepto])[:formula]
    200.times do |i|
      @c = "c" + i.to_s
      $calculadora.store(@c => 0)
    end
    $calculadora.store(liq_en_el_anho: 0)
    $calculadora.store(antiguedad: 0)
    $calculadora.store(acumulado: 0)
    $calculadora.store(salario_basico: 0)
    $calculadora.store(a1: 0)
    $calculadora.store(a2: 0)
    $calculadora.store(a3: 0)
    $calculadora.store(a4: 0)
    $calculadora.store(a5: 0)
    $calculadora.store(at: 0)
    if $calculadora.evaluate((params[:concepto])[:formula].to_s) == nil
      respond_to do |format|
        format.html { redirect_to edit_concepto_path(@concepto), error: 'La fórmula es incorrecta.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        if @concepto.update(concepto_params)
          if (params[:concepto])[:acumuladores].nil?
            Concepto.update(@concepto.id, acumuladores: Array.new)
          else
            Concepto.update(@concepto.id, acumuladores: (params[:concepto])[:acumuladores])
          end
          LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Modificar", table: "Conceptos", registro: @concepto.id, tiempo: Time.now, cambios: ["Código: " + @concepto.codigo, "Nombre: " + @concepto.nombre, "Monto: " + @concepto.monto.to_s, "Suma-Resta: " + @concepto.suma_resta, "Fórmula: " + @concepto.formula, "Tipo: " + @concepto.tipo_de_concepto, "Acumuladores: " + @concepto.acumuladores.join(", ")])
          format.html { redirect_to @concepto, notice: "El concepto ha sido editado correctamente" }
          format.json { render :show, status: :ok, location: @concepto }
        else
          format.html { render :edit }
          format.json { render json: @concepto.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /conceptos/1
  # DELETE /conceptos/1.json
  def destroy
    if !LiquidacionDetalle.where(concepto_id: @concepto.id).any? and !NovedadTransitoria.where(concepto_id: @concepto.id).any?
      LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Eliminar", table: "Conceptos", registro: @concepto.id, tiempo: Time.now, cambios: ["Código: " + @concepto.codigo, "Nombre: " + @concepto.nombre, "Monto: " + @concepto.monto.to_s, "Suma-Resta: " + @concepto.suma_resta, "Fórmula: " + @concepto.formula, "Tipo: " + @concepto.tipo_de_concepto, "Acumuladores: " + @concepto.acumuladores.join(", ")])
      @concepto.destroy
      respond_to do |format|
        format.html { redirect_to conceptos_url, notice: 'El Concepto ha sido eliminado correctamente.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to conceptos_path, error: 'No se ha podido eliminar concepto, por que tiene una o mas liquidaciones asociadas.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_concepto
      @concepto = Concepto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def concepto_params
      params.require(:concepto).permit(:codigo, :nombre, :monto, :formula, :tipo_de_concepto, :suma_resta, :estado, acumuladores: [])
    end

end
