class SalariosBasicosHistoricoController < ApplicationController
  before_action :set_salario_basico_historico, only: [:show, :edit, :update, :destroy]
  helper_method :sort_column, :sort_direction
  before_filter :authenticate_user!
  load_and_authorize_resource except: [:create]
  # GET /salarios_basicos_historico
  # GET /salarios_basicos_historico.json
  def index
    @salarios_basicos_historico = SalarioBasicoHistorico.search(params[:search]).order(sort_column + ' ' + sort_direction).paginate(:per_page => params[:per_page], :page => params[:page])
    @salario_basico_historico = SalarioBasicoHistorico.new
  end

  # GET /salarios_basicos_historico/1
  # GET /salarios_basicos_historico/1.json
  def show
  end

  # GET /salarios_basicos_historico/new
  def new
    @salario_basico_historico = SalarioBasicoHistorico.new
  end

  # GET /salarios_basicos_historico/1/edit
  def edit
    @salarios_basicos_historico = SalarioBasicoHistorico.search(params[:search]).order(sort_column + ' ' + sort_direction).paginate(:per_page => params[:per_page], :page => params[:page])
  end

  # POST /salarios_basicos_historico
  # POST /salarios_basicos_historico.json
  def create
    @salario_basico_historico = SalarioBasicoHistorico.new(salario_basico_historico_params)

    respond_to do |format|
      if @salario_basico_historico.save
        format.html { redirect_to salarios_basicos_historico_path, notice: 'Salario basico historico was successfully created.' }
        format.json { render :show, status: :created, location: @salario_basico_historico }
      else
        format.html { render :new }
        format.json { render json: @salario_basico_historico.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /salarios_basicos_historico/1
  # PATCH/PUT /salarios_basicos_historico/1.json
  def update
    respond_to do |format|
      if @salario_basico_historico.update(salario_basico_historico_params)
        format.html { redirect_to salarios_basicos_historico_path, notice: 'Salario basico historico was successfully updated.' }
        format.json { render :show, status: :ok, location: @salario_basico_historico }
      else
        format.html { render :edit }
        format.json { render json: @salario_basico_historico.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /salarios_basicos_historico/1
  # DELETE /salarios_basicos_historico/1.json
  def destroy
    @salario_basico_historico.destroy
    respond_to do |format|
      format.html { redirect_to salarios_basicos_historico_path, notice: 'Salario basico historico was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_salario_basico_historico
      @salario_basico_historico = SalarioBasicoHistorico.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def salario_basico_historico_params
      params.require(:salario_basico_historico).permit(:categoria_id, :monto, :fecha)
    end
  def sort_column
    SalarioBasicoHistorico.column_names.include?(params[:sort])? params[:sort] : 'categoria_id'
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
