class ApplicationController < ActionController::Base

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include ActionView::Helpers::NumberHelper
  protect_from_forgery with: :exception
  rescue_from CanCan::AccessDenied do |exception|
    flash[:alert] = "No estas autorizado para hacer esa accion."
    redirect_to root_url, :error => "No estas autorizado para hacer esa accion."
  end
  #Time.zone = "Buenos Aires"
  #config.time_zone = "Buenos Aires"
  add_flash_types :error
  $salario_basico = 0
  $antiguedad = 0
  $acumulado = 0
  $liq_en_el_anho = 0
  $calculadora = Dentaku::Calculator.new
  $A1 = 0
  $A2 = 0
  $A3 = 0
  $A4 = 0
  $A5 = 0
  $AT = 0
  200.times do |i|
    @c = "c" + i.to_s
    $calculadora.store(@c => 0)
  end
  $calculadora.store(a1: $A1)
  $calculadora.store(a2: $A2)
  $calculadora.store(a3: $A3)
  $calculadora.store(a4: $A4)
  $calculadora.store(a5: $A5)
  $calculadora.store(at: $AT)
  def current_controller?(names)
    names.include?(current_controller)
  end
  protected

  #derive the model name from the controller. egs UsersController will return User
  def self.permission
    return name = self.name.gsub('Controller','').singularize.split('::').last.constantize.name rescue nil
  end

  def current_ability
    @current_ability ||= Ability.new(current_user)
  end

  #load the permissions for the current user so that UI can be manipulated
  def load_permissions
    @current_permissions = current_user.role.permissions.collect{|i| [i.subject_class, i.action]}
  end
end
