class PeriodosDePagoController < ApplicationController
  before_action :set_periodo_de_pago, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  load_and_authorize_resource except: [:create, :download, :liquidar]
  def index
    @periodos_de_pago = PeriodoDePago.all
    @periodo_de_pago = PeriodoDePago.new
  end

  # GET /categorias/1
  # GET /categorias/1.json
  def show
    #setHabilitar(true)
    @periodos_de_pago = PeriodoDePago.all
  end

  # GET /categorias/new
  def new
    @periodo_de_pago = PeriodoDePago.new
  end

  # GET /categorias/1/edit
  def edit
    @periodos_de_pago = PeriodoDePago.all
  end

  # POST /categorias
  # POST /categorias.json
  def create
    @periodo_de_pago = PeriodoDePago.new(periodo_de_pago_params)
    if PeriodoDePago.where(mes: @periodo_de_pago.mes).where(anho: @periodo_de_pago.anho).first == nil
      respond_to do |format|
        if @periodo_de_pago.save
          LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Crear", table: "Periodos De Pago", registro: @periodo_de_pago.id, tiempo: Time.now, cambios: ["Periodo: " + @periodo_de_pago.mes_anho])
          format.html { redirect_to @periodo_de_pago, notice: 'El periodo de pago ha sido creado correctamente.' }
          format.json { render :show, status: :created, location: @periodo_de_pago }
        else
          format.html { render :new }
          format.json { render json: @periodo_de_pago.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to periodos_de_pago_path, error: 'El periodo de pago ya había sido creado.' }
        format.json { head :no_content }
      end
    end
  end

  # PATCH/PUT /categorias/1
  # PATCH/PUT /categorias/1.json
  def update
    respond_to do |format|
      if @periodo_de_pago.update(periodo_de_pago_params)
        format.html { redirect_to periodos_de_pago_path, notice: 'El periodo de pago ha sido actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @periodo_de_pago }
      else
        format.html { render :edit }
        format.json { render json: @periodo_de_pago.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categorias/1
  # DELETE /categorias/1.json
  def destroy
    @periodo_de_pago = PeriodoDePago.find(params[:id])
    LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Eliminar", table: "Periodos De Pago", registro: @periodo_de_pago.id, tiempo: Time.now, cambios: ["Periodo: " + @periodo_de_pago.mes_anho])
    @periodo_de_pago.destroy
    respond_to do |format|
      format.html { redirect_to periodos_de_pago_path, notice: 'El periodo de pago ha sido eliminado correctamente.' }
      format.json { head :no_content }
    end
  end

  def download
    @periodo_de_pago = PeriodoDePago.find(params[:id])
    @sumas = 0
    @descuentos = 0
    @dias_trabajados = 30
    @salario_por_dia = 0
    LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Generar", table: "Reporte de Sueldos", registro: @periodo_de_pago.id, tiempo: Time.now)
    # Assignment
    pdf = Prawn::Document.new
    @cursor = pdf.cursor
    pdf.image Rails.root.to_s + "/public" + Empresa.find(1).image_url.to_s, :width => 147, :height => 49
    pdf.move_cursor_to @cursor
    pdf.move_down 15
    pdf.font_size 7
    pdf.text "Solicitado por: " + current_user.email, :align => :right
    pdf.text Time.current.strftime("%d/%m/%Y %H:%M:%S"), :align => :right
    pdf.move_down 20
    pdf.font_size 12
    pdf.text "Reporte de Sueldos del periodo " + @periodo_de_pago.mes_anho, :align => :center
    pdf.font_size 7
    data = [["Nombre", "Apellido", "Nacionalidad", "Nº de doc.", "Dias trab.", "Salario/día", "Total sueldo", "Otros pagos", "Descuentos", " Salario Neto", "Fecha de ingreso"]]
    @periodo_de_pago.liquidacions.find_each do |liquidacion|
      pdf.stroke_horizontal_rule
      @dias_trabajados = 30
      @sumas = 0
      @descuentos = 0
      liquidacion.liquidacion_detalles.find_each do |ld|
        if ld.concepto_id == 1 #salario básico
          @salario_por_dia = (ld.suma/30.0)
        else
          @sumas += ld.suma
          @descuentos += ld.descuento
        end
      end
      liquidacion.novedades_transitorias.find_each do |nt|
        if nt.concepto_id == 2 #dias no trabajados
          @dias_trabajados -= nt.cantidad
        else
          @sumas += nt.suma
          @descuentos += nt.descuento
        end
      end
      data += [[liquidacion.trabajador.nombre, liquidacion.trabajador.apellido, liquidacion.trabajador.nacionalidad.nombre, liquidacion.trabajador.documento_de_identidad.to_s, @dias_trabajados.to_i.to_s, number_to_currency(@salario_por_dia.round(2), precision: 2, unit: "G.").to_s, number_to_currency((@salario_por_dia*@dias_trabajados).to_i, precision: 0, unit: "G.").to_s, number_to_currency(@sumas, precision: 0, unit: "G.").to_s, number_to_currency(@descuentos, precision: 0, unit: "G.").to_s, number_to_currency(liquidacion.total, precision: 0, unit: "G.").to_s, liquidacion.trabajador.fecha_de_ingreso.strftime("%d/%m/%Y") ]]
    end
    pdf.table(data, :header => true, :column_widths => {4 => 30, 10 => 60}) do
      cells.borders = []
      row(0).borders = [:bottom]
      row(0).border_width = 2
      row(0).font_style = :bold
    end
    pdf.move_up 60
    pdf.render_file "Reporte.pdf"
    send_file 'Reporte.pdf', :type=>"application/pdf", :x_sendfile=>true
  end

  def liquidar
    @total = 0
    @periodo_de_pago = PeriodoDePago.find(params[:id])
    if !params[:categoria_id].blank?
      @categoria = Categoria.find(params[:categoria_id])
      Trabajador.where(categoria_id: @categoria.id).find_each do |trabajador|
        @total = 0
        if Liquidacion.where(trabajador_id: trabajador.id, periodo_de_pago_id: @periodo_de_pago.id).first == nil and trabajador.estado == 'activo'
          @liquidacion = Liquidacion.create(trabajador_id: trabajador.id, categoria_id: trabajador.categoria_id, periodo_de_pago_id: @periodo_de_pago.id, fecha_de_pago: DateTime.now.to_date, total: 0)
          @liquidacion.cargar_variables
          Concepto.where(estado: "activo").order(codigo: :asc).fijo.each do |concepto|
            @ld = LiquidacionDetalle.create(liquidacion_id: @liquidacion.id, concepto_id: concepto.id, cantidad: 1, suma: 0, descuento: 0)
            LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Crear", table: "Liquidacion_detalles", registro: @ld.id, tiempo: Time.now, cambios: ["Liquidacion_id: " + @liquidacion.id.to_s, "Concepto_id: " + concepto.id.to_s, "Cantidad: 1", "Suma: " + number_to_currency(@ld.suma, precision: 0, unit: "G.").to_s, "Descuento: " + number_to_currency(@ld.descuento, precision: 0, unit: "G.").to_s])
            @total = @total + @ld.suma - @ld.descuento
          end
          LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Crear", table: "Liquidacion", registro: @liquidacion.id, tiempo: Time.now, cambios: ["Trabajador: " + @liquidacion.trabajador.nombre, "Periodo: " + @liquidacion.periodo_de_pago.mes_anho, "Fecha: " + @liquidacion.fecha_de_pago.to_s, "total: " + @total.to_s])
        end
      end
      redirect_to :back, notice: "Liquidaciones creadas correctamente para la categoría: " + @categoria.nombre
    else
      Trabajador.all.find_each do |trabajador|
        @total = 0
        if Liquidacion.where(trabajador_id: trabajador.id, periodo_de_pago_id: @periodo_de_pago.id).first == nil and trabajador.estado == 'activo'
          @liquidacion = Liquidacion.create(trabajador_id: trabajador.id, categoria_id: trabajador.categoria_id, periodo_de_pago_id: @periodo_de_pago.id, fecha_de_pago: DateTime.now.to_date, total: 0)
          @liquidacion.cargar_variables
          Concepto.where(estado: "activo").order(codigo: :asc).fijo.each do |concepto|
            @ld = LiquidacionDetalle.create(liquidacion_id: @liquidacion.id, concepto_id: concepto.id, cantidad: 1, suma: 0, descuento: 0)
            LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Crear", table: "Liquidacion_detalles", registro: @ld.id, tiempo: Time.now, cambios: ["Liquidacion_id: " + @liquidacion.id.to_s, "Concepto_id: " + concepto.id.to_s, "Cantidad: 1", "Suma: " + number_to_currency(@ld.suma, precision: 0, unit: "G.").to_s, "Descuento: " + number_to_currency(@ld.descuento, precision: 0, unit: "G.").to_s])
            @total = @total + @ld.suma - @ld.descuento
          end
          LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Crear", table: "Liquidacion", registro: @liquidacion.id, tiempo: Time.now, cambios: ["Trabajador: " + @liquidacion.trabajador.nombre, "Periodo: " + @liquidacion.periodo_de_pago.mes_anho, "Fecha: " + @liquidacion.fecha_de_pago.to_s, "total: " + number_to_currency(@total, precision: 0, unit: "G.").to_s])
        end
      end
      redirect_to :back, notice: "Liquidaciones creadas correctamente para todas las categorías"
    end
  end

  def finalizar
    @trabajadores = Array.new
    @error = nil
    @periodo_de_pago = PeriodoDePago.find(params[:id])
    Trabajador.where(estado: "activo").find_each do |trabajador|
      if !trabajador.liquidacions.where(periodo_de_pago_id: @periodo_de_pago.id).any?
        @error = "No se ha liquidado a todos los trabajadores activos, faltan: "
        @trabajadores.push(trabajador.nombre_apellido)
      end
    end
    if @error == nil
      @periodo_de_pago.liquidacions.find_each do |liquidacion|
        if !liquidacion.liquidacion_detalles.any?
          @error = "Faltan conceptos fijos a los trabajadores: "
          @trabajadores.push(liquidacion.trabajador.nombre_apellido)
        end
      end
    end
    if @error == nil
      PeriodoDePago.update(@periodo_de_pago.id, finalizado: TRUE )
      LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Finalizar", table: "Periodos De Pago", registro: @periodo_de_pago.id, tiempo: Time.now, cambios: ["Periodo: " + @periodo_de_pago.mes_anho])
      @asiento = AsientoContable.create(ejercicio_contable_id: 123, concepto: "Salario de " + @periodo_de_pago.mes_anho, fecha: Date.today, numero: 1112, total_debe: 0, total_haber: 0)
      @total = 0
      Liquidacion.where(periodo_de_pago_id: @periodo_de_pago.id).find_each do |liq|
        @total = @total + liq.total
      end
      AsientoDetalle.create(asiento_id: @asiento.id, cuenta_id: 1, debe: 0, haber: @total)
      AsientoDetalle.create(asiento_id: @asiento.id, cuenta_id: 2, debe: @total, haber: 0)
      redirect_to @periodo_de_pago, notice: 'Se ha finalizado correctamente el periodo de pago.'
    else
      redirect_to @periodo_de_pago, error: (@error + @trabajadores.join(', '))
    end
  end

  def generar_recibos
    @periodo_de_pago = PeriodoDePago.find(params[:id])
    pdf = Prawn::Document.new
    @contador = 1
    @paginas = Liquidacion.where(periodo_de_pago_id: @periodo_de_pago.id).count
    Liquidacion.where(periodo_de_pago_id: @periodo_de_pago.id).each do |liquidacion|
      @liquidacion = liquidacion
      data = [["Código", "Nombre", "Cantidad", "Sumas", "Descuentos"]]
      @linea = pdf.cursor
      #pdf.image "ingenium.jpg", :width => 147, :height => 49
      pdf.image Rails.root.to_s + "/public" + Empresa.find(1).image_url.to_s, :width => 147, :height => 49
      @linea2 = pdf.cursor
      pdf.move_cursor_to(@linea)
      pdf.move_down 15
      pdf.font_size 7
      pdf.text "Solicitado por: " + current_user.email, :align => :right
      pdf.text Time.current.strftime("%d/%m/%Y %H:%M:%S"), :align => :right
      pdf.move_down 20
      pdf.text "Recibo de Liquidación de Salario", :align => :center, :size => 20
      pdf.move_down 12
      pdf.font_size 10
      @linea = pdf.cursor
      @sumas = 0
      @descuentos = 0
      pdf.text "Trabajador: " + @liquidacion.trabajador.nombre_apellido
      pdf.move_cursor_to(@linea)
      pdf.text "                                           " + "Categoría: " + @liquidacion.categoria.nombre + "                   Periodo: " + @liquidacion.periodo_de_pago.mes_anho, :align => :center
      pdf.move_cursor_to(@linea)
      pdf.text "Fecha: " + @liquidacion.fecha_de_pago.strftime("%d/%m/%Y"), :align => :right
      pdf.move_down 20
      pdf.text "Detalles", :align => :center, :size => 18
      @detalles_generales = Array.new
      @liquidacion.liquidacion_detalles.find_each do |detalle|
        @detalles_generales.push(detalle)
      end
      @liquidacion.novedades_transitorias.find_each do |nt|
        @detalles_generales.push(nt)
      end
      @detalles_generales.sort! { |a, b| a.concepto.codigo.to_s <=> b.concepto.codigo.to_s }
      @detalles_generales.each do |detalles|
        data += [[detalles.concepto.codigo, detalles.concepto.nombre, detalles.cantidad.to_s, number_to_currency(detalles.suma, precision: 0, unit: "G.").to_s, number_to_currency(detalles.descuento, precision: 0, unit: "G.").to_s]]
        @sumas = @sumas + detalles.suma
        @descuentos = @descuentos + detalles.descuento
      end
      pdf.table(data, :header => true, :column_widths => {0 => 100, 1 => 245}) do
        row(0).borders = [:bottom]
        row(0).border_width = 2
        row(0).font_style = :bold
      end
      pdf.move_down 10
      @linea = pdf.cursor
      pdf.text "Total Sumas:                       .", :align => :right
      pdf.move_cursor_to(@linea)
      pdf.text number_to_currency(@sumas, precision: 0, unit: "G.").to_s, :align => :right
      @linea = pdf.cursor
      pdf.text "Total Descuentos:                       .", :align => :right
      pdf.move_cursor_to(@linea)
      pdf.text number_to_currency(@descuentos, precision: 0, unit: "G.").to_s, :align => :right
      @linea = pdf.cursor
      pdf.text "Total Neto:                       .", :align => :right
      pdf.move_cursor_to(@linea)
      pdf.text number_to_currency(@liquidacion.total, precision: 0, unit: "G.").to_s, :align => :right
      pdf.move_down 30
      pdf.text "_______________________________", :align => :right
      pdf.text "Firma", :align => :right
      if @contador < @paginas
        pdf.start_new_page
      end
      @contador = @contador + 1
    end
    LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Generar", table: "Todos los Recibos de salario", registro: @periodo_de_pago.id, tiempo: Time.now)
    pdf.render_file "Recibo de sueldo.pdf"
    send_file 'Recibo de sueldo.pdf', :type=>"application/pdf", :x_sendfile=>true
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_periodo_de_pago
    @periodo_de_pago = PeriodoDePago.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def periodo_de_pago_params
    params.require(:periodo_de_pago).permit(:mes, :anho, :finalizado)
  end

end
