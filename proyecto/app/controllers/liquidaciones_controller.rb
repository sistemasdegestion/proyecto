class LiquidacionesController < ApplicationController
  before_action :set_liquidacion, only: [:show, :edit, :update, :destroy]
  helper_method :sort_column, :sort_direction

  before_filter :authenticate_user!
  load_and_authorize_resource except: [:create, :generar_recibo, :agregar_conceptos_fijos]
  # GET /liquidaciones
  # GET /liquidaciones.json
  def index
    @liquidaciones = Liquidacion.all
    @liquidacion = Liquidacion.new
    @inhabilitar = false
  end

  # GET /liquidaciones/1
  # GET /liquidaciones/1.json
  def show
    @liquidaciones = Liquidacion.all
  end

  # GET /liquidaciones/new
  def new
    @liquidacion = Liquidacion.new
    @liquidacion.liquidacion_detalles.build
  end

  # GET /liquidaciones/1/edit
  def edit
    @liquidaciones = Liquidacion.all
    @inhabilitar = true
  end

  # POST /liquidaciones
  # POST /liquidaciones.json
  def create
    @liquidacion = Liquidacion.new(liquidacion_params)
    @liquidacion.cargar_variables
    if Liquidacion.where(trabajador_id: @liquidacion.trabajador_id).where(periodo_de_pago_id: @liquidacion.periodo_de_pago_id).first == nil #liquidación del trabajador no fue creada para ese mes
      if @liquidacion.trabajador.estado == 'activo'
        #@liquidacion.update_attribute(:total, :total + @liquidacion.liquidacion_detalles - @liquidacion.liquidacion_detalles.descuento)
        respond_to do |format|
          if @liquidacion.save
            @total = 0
            LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Crear", table: "Liquidaciones", registro: @liquidacion.id, tiempo: Time.now, cambios: ["Trabajador: " + @liquidacion.trabajador.nombre, "Periodo: " + @liquidacion.periodo_de_pago.mes_anho, "Fecha: " + Date.today.to_s, "Total: " + number_to_currency(@total, precision: 0, unit: "G.").to_s])
            format.html { redirect_to edit_liquidacion_path(@liquidacion), notice: 'La liquidación ha sido creada correctamente.'}
            format.json { render :show, status: :created, location: liquidaciones_path }
          else
            format.html { render :new }
            format.json { render json: @liquidacion.errors, status: :unprocessable_entity }
          end
        end
      else
        respond_to do |format|
          format.html { redirect_to liquidaciones_path, error: 'El trabajador ya no está activo.' }
          format.json { head :no_content }
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to liquidaciones_path, error: 'La liquidación ya estaba creada.' }
        format.json { head :no_content }
      end
    end
  end

  # PATCH/PUT /liquidaciones/1
  # PATCH/PUT /liquidaciones/1.json
  def update
    $current_user = current_user
    if @liquidacion.periodo_de_pago.finalizado?
      respond_to do |format|
        format.html { redirect_to liquidaciones_path, error: 'No se puede editar la liquidación, el periodo ya está finalizado.' }
        format.json { render :show, status: :ok, location: liquidaciones_path }
      end
    else
      respond_to do |format|
        if @liquidacion.update(liquidacion_params)
          LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Modificar", table: "Liquidaciones", registro: @liquidacion.id, tiempo: Time.now, cambios: ["Trabajador: " + @liquidacion.trabajador.nombre, "Periodo: " + @liquidacion.periodo_de_pago.mes_anho, "Fecha: " + @liquidacion.fecha_de_pago.to_s, "Total: " + number_to_currency(@liquidacion.total, precision: 0, unit: "G.").to_s])
          format.html { redirect_to @liquidacion, notice: 'La liquidación ha sido modificada correctamente.' + $AT.to_s }
          format.json { render :show, status: :ok, location: liquidaciones_path }
        else
          format.html { render :edit }
          format.json { render json: @liquidacion.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /liquidaciones/1
  # DELETE /liquidaciones/1.json
  def destroy
    @periodo_de_pago = PeriodoDePago.find(@liquidacion.periodo_de_pago_id)
    if @periodo_de_pago.finalizado == false
      LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Eliminar", table: "Liquidaciones", registro: @liquidacion.id, tiempo: Time.now, cambios: ["Trabajador: " + @liquidacion.trabajador.nombre, "Periodo: " + @liquidacion.periodo_de_pago.mes_anho, "Fecha: " + @liquidacion.fecha_de_pago.to_s, "Total: " + number_to_currency(@liquidacion.total, precision: 0, unit: "G.").to_s])
      @liquidacion.destroy
      respond_to do |format|
        format.html { redirect_to liquidaciones_url, notice: 'La liquidación ha sido eliminada correctamente.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to liquidaciones_url, error: 'No se ha podido eliminar la liquidación, el periodo de pago ha finalizado.' }
        format.json { head :no_content }
      end
    end
  end

  def agregar_conceptos_fijos
    @liquidacion = Liquidacion.find(params[:id])
    Concepto.where(estado: "activo").order(codigo: :asc).fijo.each do |concepto|
      @ld = LiquidacionDetalle.create(liquidacion_id: @liquidacion.id, concepto_id: concepto.id, cantidad: 1, suma: 0, descuento: 0)
      LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Crear", table: "Liquidacion_detalles", registro: @ld.id, tiempo: Time.now, cambios: ["Liquidacion_id: " + @liquidacion.id.to_s, "Concepto_id: " + concepto.id.to_s, "Cantidad: 1", "Suma: " + number_to_currency(@ld.suma, precision: 0, unit: "G.").to_s, "Descuento: " + number_to_currency(@ld.descuento, precision: 0, unit: "G.").to_s])
    end
    respond_to do |format|
        LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Modificar", table: "Liquidaciones", registro: @liquidacion.id, tiempo: Time.now, cambios: ["Trabajador: " + @liquidacion.trabajador.nombre, "Periodo: " + @liquidacion.periodo_de_pago.mes_anho, "Fecha: " + @liquidacion.fecha_de_pago.to_s, "Total: " + number_to_currency(@liquidacion.total, precision: 0, unit: "G.").to_s])
        format.html { redirect_to @liquidacion, notice: 'La liquidación ha sido modificada correctamente.' + $AT.to_s }
        format.json { render :show, status: :ok, location: liquidaciones_path }
    end
  end

  def generar_recibo
    @liquidacion = Liquidacion.find(params[:id])
    pdf = Prawn::Document.new
    data = [["Código", "Nombre", "Cantidad", "Sumas", "Descuentos"]]
    @linea = pdf.cursor
    #pdf.image "ingenium.jpg", :width => 147, :height => 49
    pdf.image Rails.root.to_s + "/public" + Empresa.find(1).image_url.to_s, :width => 147, :height => 49
    @linea2 = pdf.cursor
    pdf.move_cursor_to(@linea)
    pdf.move_down 15
    pdf.font_size 7
    pdf.text "Solicitado por: " + current_user.email, :align => :right
    pdf.text Time.current.strftime("%d/%m/%Y %H:%M:%S"), :align => :right
    pdf.move_down 20
    pdf.text "Recibo de Liquidación de Salario", :align => :center, :size => 20
    pdf.move_down 12
    pdf.font_size 10
    @linea = pdf.cursor
    @sumas = 0
    @descuentos = 0
    pdf.text "Trabajador: " + @liquidacion.trabajador.nombre_apellido
    pdf.move_cursor_to(@linea)
    pdf.text "                                           " + "Categoría: " + @liquidacion.categoria.nombre + "                   Periodo: " + @liquidacion.periodo_de_pago.mes_anho, :align => :center
    pdf.move_cursor_to(@linea)
    pdf.text "Fecha: " + @liquidacion.fecha_de_pago.strftime("%d/%m/%Y"), :align => :right
    pdf.move_down 20
    pdf.text "Detalles", :align => :center, :size => 18
    @detalles_generales = Array.new
    @liquidacion.liquidacion_detalles.find_each do |detalle|

      @detalles_generales.push(detalle)
    end
    @liquidacion.novedades_transitorias.find_each do |nt|

      @detalles_generales.push(nt)
    end
    @detalles_generales.sort! { |a, b| a.concepto.codigo.to_s <=> b.concepto.codigo.to_s }
    @detalles_generales.each do |detalles|
      data += [[detalles.concepto.codigo, detalles.concepto.nombre, detalles.cantidad.to_s, number_to_currency(detalles.suma, precision: 0, unit: "G.").to_s, number_to_currency(detalles.descuento, precision: 0, unit: "G.").to_s]]
      @sumas = @sumas + detalles.suma
      @descuentos = @descuentos + detalles.descuento
    end
    pdf.table(data, :header => true, :column_widths => {0 => 100, 1 => 245}) do
      row(0).borders = [:bottom]
      row(0).border_width = 2
      row(0).font_style = :bold
    end
    pdf.move_down 10
    @linea = pdf.cursor
    pdf.text "Total Sumas:                       .", :align => :right
    pdf.move_cursor_to(@linea)
    pdf.text number_to_currency(@sumas, precision: 0, unit: "G.").to_s, :align => :right
    @linea = pdf.cursor
    pdf.text "Total Descuentos:                       .", :align => :right
    pdf.move_cursor_to(@linea)
    pdf.text number_to_currency(@descuentos, precision: 0, unit: "G.").to_s, :align => :right
    @linea = pdf.cursor
    pdf.text "Total Neto:                       .", :align => :right
    pdf.move_cursor_to(@linea)
    pdf.text number_to_currency(@liquidacion.total, precision: 0, unit: "G.").to_s, :align => :right
    pdf.move_down 30
    pdf.text "_______________________________", :align => :right
    pdf.text "Firma", :align => :right
    pdf.render_file "Recibo de sueldo.pdf"
    LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Generar", table: "Recibo de salario", registro: @liquidacion.id, tiempo: Time.now)
    send_file 'Recibo de sueldo.pdf', :type=>"application/pdf", :x_sendfile=>true
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_liquidacion
    @liquidacion = Liquidacion.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def liquidacion_params
    params.require(:liquidacion).permit(:trabajador_id, :categoria_id, :periodo_de_pago_id, :fecha_de_pago, :total,
                                        liquidacion_detalles_attributes: [:id, :concepto_id, :cantidad, :suma, :descuento, :_destroy],
                                        novedades_transitorias_attributes: [:id, :concepto_id, :cantidad, :suma, :descuento, :_destroy])
  end
  def sort_column
    Liquidacion.column_names.include?(params[:sort])? params[:sort] : 'trabajador_id'
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
