class RolesController < ApplicationController
  before_action :set_role, only: [:show, :edit, :update, :destroy]

  before_filter :authenticate_user!
  load_and_authorize_resource except: [:create]
  # GET /liquidaciones
  # GET /liquidaciones.json
  def index
    @roles = Role.all
    @role = Role.new
    @modelos = ['Categoria', 'Concepto', 'Empresa', 'Liquidacion','LogAutitoria',
                'PeriodoDePago', 'Trabajador', 'User', 'Role', 'all']
    @modeloslindos = ['Categorias', 'Conceptos', 'Empresa', 'Liquidaciones','Log de Auditoria',
                'Periodos de pago', 'Trabajadores', 'Usuarios', 'Roles', 'todos']
    10.times { @role.permissions.build }
  end

  # GET /liquidaciones/1
  # GET /liquidaciones/1.json
  def show
    @roles = Role.all
  end

  # GET /liquidaciones/new
  def new
    @role = Role.new
  end

  # GET /liquidaciones/1/edit
  def edit
    @roles = Role.all
    @modelos = ['Categoria', 'Concepto', 'Empresa', 'Liquidacion','LogAutitoria',
                'PeriodoDePago', 'Trabajador', 'User', 'Role', 'all']
    @modeloslindos = ['Categorias', 'Conceptos', 'Empresa', 'Liquidaciones','Log de Auditoria',
                      'Periodos de pago', 'Trabajadores', 'Usuarios', 'Roles', 'todos']
  end

  # POST /liquidaciones
  # POST /liquidaciones.json
  def create
    @role = Role.new(role_params)
        respond_to do |format|
          if @role.save
            LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Crear", table: "Roles", registro: @role.id, tiempo: Time.now, cambios: ["Nombre: " + @role.name])

            format.html { redirect_to @role, notice: 'El rol ha sido creado correctamente.' }
            format.json { render :show, status: :created, location: roles_path }
          else
            format.html { render :new }
            format.json { render json: @role.errors, status: :unprocessable_entity }
          end
        end
  end

  # PATCH/PUT /liquidaciones/1
  # PATCH/PUT /liquidaciones/1.json
  def update
    $current_user = current_user
    respond_to do |format|
      if @role.update(role_params)
        LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Actualizar", table: "Roles", registro: @role.id, tiempo: Time.now, cambios: ["Nombre: " + @role.name])
        format.html { redirect_to @role, notice: 'El rol ha sido modificado correctamente.' }
        format.json { render :show, status: :ok, location: liquidaciones_path }
      else
        format.html { render :edit }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /liquidaciones/1
  # DELETE /liquidaciones/1.json
  def destroy
      if @role.users.any?
        respond_to do |format|
          format.html { redirect_to roles_url, error: 'No se ha podido eliminar el rol, corresponde a uno o mas usuarios.' }
          format.json { head :no_content }
        end
      else
        LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Eliminar", table: "Roles", registro: @role.id, tiempo: Time.now, cambios: ["Nombre: " + @role.name])
        @role.destroy
        respond_to do |format|
          format.html { redirect_to roles_url, notice: 'El rol ha sido eliminada correctamente.' }
          format.json { head :no_content }
        end
      end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_role
    @role = Role.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def role_params
    params.require(:role).permit(:name, permissions_attributes: [:id, :subject_class, :_destroy, actions: []])
  end

end
