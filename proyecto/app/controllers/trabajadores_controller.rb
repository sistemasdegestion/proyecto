class TrabajadoresController < ApplicationController
  before_action :set_trabajador, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  load_and_authorize_resource except: [:create]
  def index
    @trabajadores = Trabajador.all
    @trabajador = Trabajador.new
  end

  # GET /trabajadores/1
  # GET /trabajadores/1.json
  def show
    @trabajadores = Trabajador.all
  end

  # GET /trabajadores/new
  def new
    @trabajador = Trabajador.new
  end

  # GET /trabajadores/1/edit
  def edit
    @trabajadores = Trabajador.all
  end

  # POST /trabajadores
  # POST /trabajadores.json
  def create
    @trabajador = Trabajador.new(trabajador_params)
    respond_to do |format|
      if @trabajador.save
        LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Crear", table: "Trabajadores", registro: @trabajador.id, tiempo: Time.now, cambios: ["nombre: " + @trabajador.nombre, "Apellido: " + @trabajador.apellido, "Ciudad: " + @trabajador.ciudad, "Nacionalidad: " + @trabajador.nacionalidad.nombre, "Documento de identidad: " + @trabajador.documento_de_identidad.to_s, "email: " + @trabajador.email, "Telefono: " + @trabajador.numero_de_telefono, "Categoría: " + @trabajador.categoria.nombre, "Estado: " + @trabajador.estado, "Direccion: " + @trabajador.direccion, "Fecha de ingreso: " + @trabajador.fecha_de_ingreso.to_s] )
        format.html { redirect_to @trabajador, notice: 'El Trabajador ha sido creado correctamente.' }
        format.json { render :show, status: :created, location: @trabajador }
        format.js { render :create, status: :created, location: @trabajador }
      else
        format.html { render :new }
        format.json { render json: @trabajador.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trabajadores/1
  # PATCH/PUT /trabajadores/1.json
  def update
    respond_to do |format|
      @trabajador = Trabajador.find(params[:id])
      @cambios = []
      if @trabajador.update(trabajador_params)
        @cambios.push("Nombre: " + @trabajador.nombre)
        @cambios.push("Apellido: " + @trabajador.apellido)
        @cambios.push("Ciudad: " + @trabajador.ciudad)
        @cambios.push("Documento de identidad: " + @trabajador.documento_de_identidad.to_s)
        @cambios.push("email: " + @trabajador.email)
        @cambios.push("Telefono: " + @trabajador.numero_de_telefono)
        @cambios.push("Categoría: " + @trabajador.categoria.nombre)
        @cambios.push("Estado: " + @trabajador.estado)
        @cambios.push("Direccion: " + @trabajador.direccion)
        @cambios.push("Fecha de ingreso: " + @trabajador.fecha_de_ingreso.to_s)
        LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Modificar", table: "Trabajadores", registro: @trabajador.id, tiempo: Time.now, cambios: @cambios)
        format.html { redirect_to @trabajador, notice: 'El Trabajador ha sido modificado correctamente.' }
        format.json { render :show, status: :ok, location: @trabajador }
      else
        format.html { render :edit }
        format.json { render json: @trabajador.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trabajadores/1
  # DELETE /trabajadores/1.json
  def destroy
    if !Liquidacion.where(trabajador_id: @trabajador.id).any?
      LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Eliminar", table: "Trabajadores", registro: @trabajador.id, tiempo: Time.now, cambios: ["nombre: " + @trabajador.nombre, "Apellido: " + @trabajador.apellido, "Ciudad: " + @trabajador.ciudad, "Nacionalidad: " + @trabajador.nacionalidad.nombre, "Documento de identidad: " + @trabajador.documento_de_identidad.to_s, "email: " + @trabajador.email, "Telefono: " + @trabajador.numero_de_telefono, "Categoría: " + @trabajador.categoria.nombre, "Estado: " + @trabajador.estado, "Direccion: " + @trabajador.direccion, "Fecha de ingreso: " + @trabajador.fecha_de_ingreso.to_s])
      @trabajador.destroy
      respond_to do |format|

        format.html { redirect_to trabajadores_path, notice: 'El Trabajador ha sido eliminado correctamente.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to trabajadores_path, error: 'No se ha podido eliminar trabajador, por que tiene una o mas liquidaciones asociadas.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajador
      @trabajador = Trabajador.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajador_params
      params.require(:trabajador).permit(:nombre,:apellido, :ciudad, :direccion, :nacionalidad_id, :documento_de_identidad, :email, :numero_de_telefono, :categoria_id, :fecha_de_ingreso, :estado)
    end
end
