class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  load_and_authorize_resource except: [:create]
  def index
    @users = User.all
  end

  # GET /categorias/1
  # GET /categorias/1.json
  def show
    #setHabilitar(true)
    @users = User.all
  end

  # GET /categorias/new
  def new

  end

  # GET /categorias/1/edit
  def edit
    @users = User.all
  end

  # POST /categorias
  # POST /categorias.json
  def create

  end

  # PATCH/PUT /categorias/1
  # PATCH/PUT /categorias/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        LogAutitoria.create(user_name: current_user.email, role: current_user.role.name, action: "Modificar", table: "Usuario", registro: @user.id, tiempo: Time.now)
        format.html { redirect_to @user, notice: 'EL usuario ha sido actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @user}
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categorias/1
  # DELETE /categorias/1.json
  def destroy

  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:role_id)
  end
end
