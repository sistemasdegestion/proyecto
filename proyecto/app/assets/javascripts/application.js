// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
//= require bootstrap-sprockets
//= require jquery_nested_form
//= require sb-admin-2

//= require rails.validations
//= require rails.validations.simple_form
//= require rails.validations.nested_form
//= require dataTables/jquery.dataTables
//= require dataTables/bootstrap/3/jquery.dataTables.bootstrap
//= require bootstrap/modal
//= require bootstrap-datepicker/core

$.fn.datepicker.dates['en'] = {
    days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
    daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá", "Do"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    startDate: new Date('01/01/2012'),
    endDate: new Date('01/01/2013')
};

(function($){
    $.fn.validettaLanguage = {};
    $.validettaLanguage = {
        init : function(){
            $.validettaLanguage.messages = {
                required	: 'Campo obligatorio.',
                email		: 'Correo electrónico no válido.',
                number		: 'Este campo sólo acepta valores numéricos.',
                maxLength	: 'Este campo acepta como máximo {count} caracteres.',
                minLength	: 'Este campo requiere como mínimo {count} caracteres.',
                maxChecked	: 'Sólo se puede marcar {count} opciones como máximo.',
                minChecked	: 'Es necesario marcar como mínimo {count} opciones.',
                maxSelected	: 'Sólo se puede marcar {count} opciones como máximo.',
                minSelected	: 'Es necesario marcar como mínimo {count} opciones.',
                notEqual	: 'Los campos no coinciden.',
                different   : 'Fields cannot be the same as each other',
                creditCard	: 'Tarjeta de crédito no válida.'
            };
        }
    };
    $.validettaLanguage.init();
})(jQuery);