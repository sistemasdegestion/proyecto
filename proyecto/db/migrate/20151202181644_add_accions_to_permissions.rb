class AddAccionsToPermissions < ActiveRecord::Migration
  def change
    add_column :permissions, :actions, :text, array:true, default: []
  end
end
