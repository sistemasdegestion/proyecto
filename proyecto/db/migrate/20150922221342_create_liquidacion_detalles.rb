class CreateLiquidacionDetalles < ActiveRecord::Migration
  def change
    create_table :liquidacion_detalles do |t|
      t.integer :liquidacion_id
      t.integer :concepto_id
      t.decimal :cantidad
      t.decimal :suma
      t.decimal :descuento

      t.timestamps
    end
  end
end
