class CreateLogAuditorias < ActiveRecord::Migration
  def change
    create_table :log_auditorias do |t|
      t.string :user_name
      t.string :role
      t.string :action
      t.string :table
      t.integer :registro

      t.timestamps
    end
  end
end
