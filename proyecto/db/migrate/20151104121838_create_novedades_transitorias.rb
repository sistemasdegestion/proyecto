class CreateNovedadesTransitorias < ActiveRecord::Migration
  def change
    create_table :novedades_transitorias do |t|
      t.integer :liquidacion_id
      t.string :concepto_id_integer
      t.decimal :cantidad
      t.decimal :suma
      t.decimal :descuento

      t.timestamps
    end
  end
end
