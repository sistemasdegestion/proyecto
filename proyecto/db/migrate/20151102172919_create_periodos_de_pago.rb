class CreatePeriodosDePago < ActiveRecord::Migration
  def change
    create_table :periodos_de_pago do |t|
      t.string :mes
      t.string :anho
      t.boolean :finalizado

      t.timestamps
    end
  end
end
