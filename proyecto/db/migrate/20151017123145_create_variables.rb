class CreateVariables < ActiveRecord::Migration
  def change
    create_table :variables do |t|
      t.string :nombre
      t.integer :valor

      t.timestamps
    end
  end
end
