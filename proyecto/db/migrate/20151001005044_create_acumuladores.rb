class CreateAcumuladores < ActiveRecord::Migration
  def change
    create_table :acumuladores do |t|
      t.string :codigo
      t.integer :variable

      t.timestamps
    end
  end
end
