class CreateAsientosContables < ActiveRecord::Migration
  def change
    create_table :asientos_contables do |t|
      t.integer :ejercicio_contable_id
      t.string :concepto
      t.date :fecha
      t.decimal :numero
      t.decimal :total_debe
      t.decimal :total_haber

      t.timestamps
    end
  end
end
