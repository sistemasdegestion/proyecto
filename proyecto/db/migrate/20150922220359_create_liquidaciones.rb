class CreateLiquidaciones < ActiveRecord::Migration
  def change
    create_table :liquidaciones do |t|
      t.integer :trabajador_id
      t.integer :categoria_id
      t.string :mes
      t.date :fecha_de_pago
      t.decimal :total

      t.timestamps
    end
  end
end
