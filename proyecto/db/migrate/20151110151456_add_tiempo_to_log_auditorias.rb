class AddTiempoToLogAuditorias < ActiveRecord::Migration
  def change
    add_column :log_auditorias, :tiempo, :timestamp
  end
end
