class CambioColumaNombre < ActiveRecord::Migration
  def change
    change_column :trabajadores, :ciudad, :string
    change_column :trabajadores, :estado_id, :string
  end
end
