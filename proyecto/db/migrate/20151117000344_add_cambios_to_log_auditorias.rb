class AddCambiosToLogAuditorias < ActiveRecord::Migration
  def change
    add_column :log_auditorias, :cambios, :text, array:true, default: []
  end
end
