class CreateSalariosBasicosHistorico < ActiveRecord::Migration
  def change
    create_table :salarios_basicos_historico do |t|
      t.integer :categoria_id
      t.decimal :monto
      t.date :fecha

      t.timestamps
    end
  end
end
