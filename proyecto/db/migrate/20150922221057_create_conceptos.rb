class CreateConceptos < ActiveRecord::Migration
  def change
    create_table :conceptos do |t|
      t.string :codigo
      t.string :nombre
      t.decimal :monto
      t.string :formula
      t.string :tipo_de_concepto
      t.string :acumulador
      t.string :suma_resta

      t.timestamps
    end
  end
end
