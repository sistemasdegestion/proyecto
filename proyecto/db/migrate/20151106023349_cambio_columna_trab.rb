class CambioColumnaTrab < ActiveRecord::Migration
  def up
    remove_column :trabajadores, :documento_de_identidad
  end

  def down
    add_column :trabajadores, :documento_de_identidad, :integer
    add_column :trabajadores, :apellido, :string
  end
end
