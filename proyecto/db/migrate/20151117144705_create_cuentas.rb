class CreateCuentas < ActiveRecord::Migration
  def change
    create_table :cuentas do |t|
      t.decimal :numero
      t.string :descripcion
      t.decimal :nivel

      t.timestamps
    end
  end
end
