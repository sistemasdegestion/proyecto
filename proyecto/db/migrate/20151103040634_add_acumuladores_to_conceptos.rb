class AddAcumuladoresToConceptos < ActiveRecord::Migration
  def change
    add_column :conceptos, :acumuladores, :text, array:true, default: []
  end
end
