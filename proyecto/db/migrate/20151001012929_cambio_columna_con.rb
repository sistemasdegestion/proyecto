class CambioColumnaCon < ActiveRecord::Migration
  def up
    remove_column :conceptos, :acumulador
  end

  def down
    add_column :conceptos, :acumulador_id, :integer
  end
end
