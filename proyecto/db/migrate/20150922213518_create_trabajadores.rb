class CreateTrabajadores < ActiveRecord::Migration
  def change
    create_table :trabajadores do |t|
      t.string :nombre
      t.integer :ciudad
      t.string :direccion
      t.integer :nacionalidad
      t.string :documento_de_identidad
      t.string :email
      t.string :numero_de_telefono
      t.integer :categoria_id
      t.date :fecha_de_ingreso
      t.integer :estado_id

      t.timestamps
    end
  end
end
