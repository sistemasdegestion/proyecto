class AddConceptoIdToNovedadesTransitorias < ActiveRecord::Migration
  def change
    add_column :novedades_transitorias, :concepto_id, :integer
  end
end
