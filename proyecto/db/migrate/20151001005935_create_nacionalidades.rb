class CreateNacionalidades < ActiveRecord::Migration
  def change
    create_table :nacionalidades do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
