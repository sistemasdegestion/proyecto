class CreateEmpresas < ActiveRecord::Migration
  def change
    create_table :empresas do |t|
      t.string :nombre
      t.string :direccion
      t.string :correo
      t.string :telefono
      t.string :image
      t.string :remote_image_url

      t.timestamps
    end
  end
end
