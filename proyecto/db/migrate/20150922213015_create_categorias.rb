class CreateCategorias < ActiveRecord::Migration
  def change
    create_table :categorias do |t|
      t.string :nombre
      t.decimal :salario_basico

      t.timestamps
    end
  end
end
