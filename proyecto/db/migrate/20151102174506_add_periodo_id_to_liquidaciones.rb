class AddPeriodoIdToLiquidaciones < ActiveRecord::Migration
  def change
    add_column :liquidaciones, :periodo_id, :integer
  end
end
