class CreateParametros < ActiveRecord::Migration
  def change
    create_table :parametros do |t|
      t.string :nombre
      t.integer :valor

      t.timestamps
    end
  end
end
