class CreateAsientoDetalles < ActiveRecord::Migration
  def change
    create_table :asiento_detalles do |t|
      t.integer :asiento_id
      t.integer :cuenta_id
      t.decimal :debe
      t.decimal :haber

      t.timestamps
    end
  end
end
