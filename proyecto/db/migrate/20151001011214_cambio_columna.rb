class CambioColumna < ActiveRecord::Migration
  def self.up
    rename_column :trabajadores, :nacionalidad, :nacionalidad_id
  end

  def self.down

  end
end
