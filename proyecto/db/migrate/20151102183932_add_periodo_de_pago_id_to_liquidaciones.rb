class AddPeriodoDePagoIdToLiquidaciones < ActiveRecord::Migration
  def change
    add_column :liquidaciones, :periodo_de_pago_id, :integer
  end
end
