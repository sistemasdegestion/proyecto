# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160205134749) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "acumuladores", force: true do |t|
    t.string   "codigo"
    t.integer  "variable",   default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "asiento_detalles", force: true do |t|
    t.integer  "asiento_id"
    t.integer  "cuenta_id"
    t.decimal  "debe"
    t.decimal  "haber"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "asientos_contables", force: true do |t|
    t.integer  "ejercicio_contable_id"
    t.string   "concepto"
    t.date     "fecha"
    t.decimal  "numero",                precision: 8, scale: 0
    t.decimal  "total_debe"
    t.decimal  "total_haber"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categoria", force: true do |t|
    t.string   "nombre"
    t.decimal  "sueldo_basico", precision: 10, scale: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categorias", force: true do |t|
    t.string   "nombre"
    t.decimal  "salario_basico", precision: 10, scale: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "conceptos", force: true do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.decimal  "monto",            precision: 10, scale: 0, default: 0,   null: false
    t.text     "formula",                                   default: "0"
    t.string   "tipo_de_concepto"
    t.string   "suma_resta"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "acumulador_id"
    t.text     "acumuladores",                              default: [],               array: true
    t.string   "estado"
  end

  create_table "cuentas", force: true do |t|
    t.decimal  "numero"
    t.string   "descripcion"
    t.decimal  "nivel"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "empresas", force: true do |t|
    t.string   "nombre"
    t.string   "direccion"
    t.string   "correo"
    t.string   "telefono"
    t.string   "image"
    t.string   "remote_image_url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "liquidacion_detalles", force: true do |t|
    t.integer  "liquidacion_id"
    t.integer  "concepto_id"
    t.decimal  "cantidad",       precision: 6,  scale: 2, default: 1.0, null: false
    t.decimal  "suma",           precision: 10, scale: 0, default: 0,   null: false
    t.decimal  "descuento",      precision: 10, scale: 0, default: 0,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "liquidacion_detalles", ["concepto_id"], name: "fki_concepto_id(liquidacion_detalles)", using: :btree
  add_index "liquidacion_detalles", ["liquidacion_id"], name: "fki_liquidacion_id(liquidacion_detalles)", using: :btree

  create_table "liquidaciones", force: true do |t|
    t.integer  "trabajador_id"
    t.integer  "categoria_id"
    t.string   "mes"
    t.date     "fecha_de_pago"
    t.decimal  "total",              precision: 10, scale: 0, default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "periodo_id"
    t.integer  "periodo_de_pago_id"
  end

  add_index "liquidaciones", ["categoria_id"], name: "fki_categoria_id(liquidaciones)", using: :btree
  add_index "liquidaciones", ["periodo_de_pago_id"], name: "fki_periodo_de_pago_id(liquidaciones)", using: :btree
  add_index "liquidaciones", ["trabajador_id"], name: "fki_trabajador_id(liquidaciones)", using: :btree

  create_table "log_auditorias", force: true do |t|
    t.string   "user_name"
    t.string   "role"
    t.string   "action"
    t.string   "table"
    t.integer  "registro"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "tiempo"
    t.text     "cambios",    default: [], array: true
  end

  create_table "nacionalidades", force: true do |t|
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "novedades_transitorias", force: true do |t|
    t.integer  "liquidacion_id"
    t.string   "concepto_id_integer"
    t.decimal  "cantidad",                                     default: 1.0, null: false
    t.decimal  "suma",                precision: 10, scale: 0, default: 0,   null: false
    t.decimal  "descuento",           precision: 10, scale: 0, default: 0,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "concepto_id"
  end

  add_index "novedades_transitorias", ["liquidacion_id"], name: "fki_liquidacion_id(novedades_transitorias)", using: :btree

  create_table "parametros", force: true do |t|
    t.string   "nombre"
    t.integer  "valor"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "periodos_de_pago", force: true do |t|
    t.string   "mes"
    t.string   "anho"
    t.boolean  "finalizado", default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "permissions", force: true do |t|
    t.integer  "role_id"
    t.string   "subject_class"
    t.string   "action"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "actions",       default: [], array: true
  end

  create_table "roles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "salarios_basicos_historico", force: true do |t|
    t.integer  "categoria_id"
    t.decimal  "monto",        precision: 10, scale: 0
    t.date     "fecha",                                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "salarios_basicos_historico", ["categoria_id"], name: "fki_categoria_id(salarios_basicos_historico)", using: :btree

  create_table "trabajadores", force: true do |t|
    t.string   "nombre"
    t.string   "ciudad"
    t.string   "direccion"
    t.integer  "nacionalidad_id"
    t.string   "email"
    t.string   "numero_de_telefono"
    t.integer  "categoria_id"
    t.date     "fecha_de_ingreso"
    t.string   "estado"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "documento_de_identidad"
    t.string   "apellido"
  end

  add_index "trabajadores", ["categoria_id"], name: "fki_categoria_id(trabajadores)", using: :btree
  add_index "trabajadores", ["nacionalidad_id"], name: "fki_nacionalidad_id(trabajadores)", using: :btree

  create_table "trabajadors", force: true do |t|
    t.string   "nombre"
    t.string   "domicilio"
    t.string   "documento_de_identidad"
    t.integer  "categoria_id"
    t.date     "fecha_de_ingreso"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "role"
    t.integer  "role_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "variables", force: true do |t|
    t.string   "nombre"
    t.integer  "valor",      default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
