Rails.application.routes.draw do

  resources :empresas

  resources :asiento_detalles

  resources :asientos_contables

  #match "/periodos_de_pago/liquidar " => "periodos_de_pago#liquidar", :as => 'liquidar'

  resources :novedades_transitorias

  resources :periodos_de_pago do
    collection do
      post 'liquidar'
      get 'liquidar'
      get 'generar_recibos'
      post 'generar_recibos'
    end
    collection do
      get 'finalizar'
      get 'download'
    end
  end

  devise_for :users, :controllers => {:sessions => "custom_sessions"}

  resources :recibo_detalles

  resources :recibo_de_sueldos

  resources :salarios_basicos_historico

  resources :liquidacion_detalles

  resources :conceptos

  resources :liquidaciones do
    collection do
      post 'generar_recibo'
      post 'agregar_conceptos_fijos'
      get 'generar_recibo'
      get 'agregar_conceptos_fijos'
    end
  end

  resources :trabajadores

  resources :categorias

  resources :nacionalidades

  resources :log_auditorias

  resources :users

  resources :roles

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root to: 'pantalla_inicial#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
