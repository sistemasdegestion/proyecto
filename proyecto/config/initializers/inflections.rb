# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:
ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
  inflect.irregular 'log_autitoria', 'log_auditorias'
  inflect.irregular 'personal', 'personales'
  inflect.irregular 'sueldo', 'sueldos'
  inflect.irregular 'detalle', 'detalles'
  inflect.irregular 'categoria', 'categorias'
  inflect.irregular 'salario_basico_historico', 'salarios_basicos_historico'
  inflect.irregular 'liquidacion', 'liquidaciones'
  inflect.irregular 'trabajador', 'trabajadores'
  inflect.irregular 'liquidacion_detalle', 'liquidacion_detalles'
  inflect.irregular 'concepto', 'conceptos'
  inflect.irregular 'estado_de_trabajador', 'estados_de_trabajador'
  inflect.irregular 'acumulador', 'acumuladores'
  inflect.irregular 'nacionalidad', 'nacionalidades'
  inflect.irregular 'parametro', 'parametros'
  inflect.irregular 'periodo_de_pago', 'periodos_de_pago'
  inflect.irregular 'novedad_transitoria', 'novedades_transitorias'
  inflect.irregular 'asiento_contable', 'asientos_contables'
  inflect.irregular 'asiento_detalle', 'asiento_detalles'
  inflect.irregular 'cuenta', 'cuentas'
end
# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end
